import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RoomService {

  RoomURL="http://localhost:8080/api/room/";
  roomURLByUser="http://localhost:8080/api/room/user/"
  constructor(
    private http: HttpClient
  ) { }
  createRoom(params){
    console.log(params)
    return this.http.post(this.RoomURL, params);
  }
  getListRoom(){
    return this.http.get(this.RoomURL);
  }
  getListRoomByUser(createBy){
    return this.http.get(`${this.roomURLByUser}${createBy}/`);
  }
  getRoomById(id){
    return this.http.get(`${this.RoomURL}${id}/`);
  }
  deleteRoom(id){
    return this.http.delete(`${this.RoomURL}${id}/`);
  }
  updateRoom(id, value){
    return this.http.put(`${this.RoomURL}${id}/`, value);
  }
}
