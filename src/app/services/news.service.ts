import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  newsURL = "http://localhost:8080/api/news/";



  constructor(
    private http: HttpClient
  ) { }
  getListMynews() {
    return this.http.get(this.newsURL);
  }
  deleteMynews(id) {

  }
  createNew(params) {
    console.log(params)
    return this.http.post(this.newsURL, params);
  }

}
