import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MynewsService {
  newsURL = "http://localhost:8080/api/news/getByUser/";



  constructor(
    private http: HttpClient
  ) { }
  createMynews(params) {
    console.log(params)
    return this.http.post(this.newsURL, params);
  }
  getListMynews() {
    return this.http.get(this.newsURL)
      .pipe(map((data: any) => data.results
      ));
  }
  deleteMynews(id) {

  }
  createNew(params) {
    console.log(params)
    return this.http.post(this.newsURL, params);
  }
}
