import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  isLoggedInSuccess = false;
  registerURL="http://localhost:8080/api/auth/signup";
  logonURL="http://localhost:8080/api/auth/signin";
  constructor(
    private http: HttpClient
  ) { }
  logon(params) {
    return this.http.post(this.logonURL,params)
  }
  getUserName(Headers) {
    return this.http.post(this.logonURL,Headers)
  }
 
  getUser() {
    var user = localStorage.getItem("user");
    if (!!!user) {
      user = sessionStorage.getItem("user");
    }
    return JSON.parse(user);
  }
  register(params){
    return this.http.post(this.registerURL,params); 
  }
  isLoggedIn() {
    if (this.getUser()) {
      this.isLoggedInSuccess = true;
      return true;
    }
    
    return false;
  }
  
}
