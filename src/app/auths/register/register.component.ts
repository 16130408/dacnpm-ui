import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(
    public userService: UserService,
    public router: Router
  ) { }
  register_info: any = {};
  error = "";
  formRegister: FormGroup
  ngOnInit(): void {
    this.formRegister = new FormGroup({
      username: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required,Validators.minLength(6)]),
      confirm_password: new FormControl("", [Validators.required,Validators.minLength(6)]),
      email: new FormControl("", [Validators.required,Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])
    })
  }
  formchange() {
    this.formRegister.valueChanges.subscribe(data => {
      if (data) {
        this.error = "";
      }
    })
  }
  onSubmit() {
    if (this.formRegister.value['password'] != this.formRegister.value['confirm_password']) {
      this.error = "Password and Confirm password are not match."
    } else if (this.formRegister.valid) {
      this.register_info = {
        "username": this.formRegister.value['username'],
        "email": this.formRegister.value['email'],
        "password": this.formRegister.value['password'],
        "role": ["user"]
      }
      console.log(this.register_info)
      this.userService.register(this.register_info)
        .pipe(
          catchError((data) => {
            console.log(data)
            if (data.error) {
              this.error = data.error.message;
            } else {
              this.error = '';
            }
            return of();
          })
        )
        .subscribe((data: any) => {
          if (data.error) {
            this.error = data.error;
          } else {
            this.router.navigate(["logon"]);
          }
        })
    }
  }
}
