import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogonRoutingModule } from './logon-routing.module';
import { LogonComponent } from './logon.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import { HttpClientModule } from '@angular/common/http';
@NgModule({
  declarations: [LogonComponent],
  imports: [
    CommonModule,
    LogonRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    HttpClientModule
  ],
  exports:[
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule
  ]
})
export class LogonModule { }
