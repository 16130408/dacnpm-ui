import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-logon',
  templateUrl: './logon.component.html',
  styleUrls: ['./logon.component.scss']
})
export class LogonComponent implements OnInit {

  constructor(
    public userService: UserService,
    public router: Router
  ) { }
  formLogon: FormGroup
  
  error= "";

  ngOnInit(): void {
    this.formLogon = new FormGroup({
      username: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required])
    });
  }
  onSubmit() {
    console.log(this.formLogon.value);
    if (this.formLogon.valid) {
      this.userService.logon(this.formLogon.value)
      .pipe(
        catchError((data) => {
          console.log(data)
          if (data.error) {
            this.error = data.error.message;
          } else {
            this.error = '';
          }
          return of();
        })
      )
      .subscribe((data: any)=>{
        if(data){
          sessionStorage.setItem('user', JSON.stringify(data));
          this.router.navigate(["/home"]);
        }else {
          this.error = "User name and Password are invalid";
          console.log(this.error)
        }
      })
    }
  }
}
