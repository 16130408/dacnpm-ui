import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { MatSliderModule } from '@angular/material/slider';
import { ReactiveFormsModule } from '@angular/forms';
import { LogonModule } from './auths/logon/logon.module';
import { RegisterModule } from './auths/register/register.module';
import { RoomModule } from './room/room.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { TokenInterceptor } from './token-interceptor';
import { MyroomModule } from './myroom/myroom.module';
import { NewsModule} from  './news/news.module';
import { MyNewsModule } from './my-news/my-news.module';
import { MenuComponent } from './menu/menu.component';


// import { AlertComponent } from './alert/alert.component';
@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    // AlertComponent


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatInputModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    LogonModule,
    RegisterModule,
    RoomModule,
    MyroomModule,    
    HttpClientModule,
    MyNewsModule,
    NewsModule,
  ],
  providers: [
    HttpClientModule,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
