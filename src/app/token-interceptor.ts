import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { UserService } from './services/user.service';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(
    public userService: UserService,
    private router: Router
  ) {

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const user = this.userService.getUser();
    let modifiedReq;
    if (user) {
      modifiedReq = req.clone({
        headers: req.headers
          .set('Authorization', `Bearer ${user.accessToken}`)
          // .set('Content-Type', "application/json")
      });
    } else {
      modifiedReq = req.clone({
        headers: req.headers
          // .set('Content-Type', "application/json")
      });
    }

    return next.handle(modifiedReq).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {

        }
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        if (error.status == 401) {
          sessionStorage.removeItem("user");
          this.router.navigate(['/']);
        }
        return throwError(error);
      })
    )
  }
}