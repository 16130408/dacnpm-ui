import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { UserService } from './services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'angular-spring';
  user: any;
  constructor(
    public userService: UserService,
    private router: Router,
  ){
    this.user = this.userService.getUser();
    console.log(this.user);
    
  }

  ngOnInit(): void {
    
  }
  logout(){
    console.log("ookfsadfjskdf")
    sessionStorage.removeItem("user");
    this.router.navigate([""]);
  }
}
