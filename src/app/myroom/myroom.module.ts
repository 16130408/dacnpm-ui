import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyroomRoutingModule } from './myroom-routing.module';
import { MatDialogModule } from '@angular/material/dialog';
import { CdkTableModule } from '@angular/cdk/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { ReactiveFormsModule } from '@angular/forms';
import { MyroomComponent } from './myroom.component';
import { UpdateRoomComponent } from './update-room/update-room.component';

@NgModule({
  declarations: [MyroomComponent, UpdateRoomComponent],
  imports: [
    CommonModule,
    MyroomRoutingModule,
    MatDialogModule,
    CdkTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatSelectModule,
    ReactiveFormsModule
  ]
})
export class MyroomModule { }
