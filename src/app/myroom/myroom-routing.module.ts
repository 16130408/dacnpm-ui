import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyroomComponent } from './myroom.component';
import { UpdateRoomComponent } from './update-room/update-room.component';
const routes: Routes = [
  { path: '', component: MyroomComponent },
  { path: 'update-room/:room_id', component: UpdateRoomComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyroomRoutingModule { }
