import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { RoomService } from '../services/room.service';

@Component({
  selector: 'app-myroom',
  templateUrl: './myroom.component.html',
  styleUrls: ['./myroom.component.scss']
})
export class MyroomComponent implements OnInit {
  user: any;
  constructor(
    public userService: UserService,
    public roomService: RoomService
  ) { 
    this.user = userService.getUser();
  }

  ngOnInit(): void {
    this.getListRoom();
  }
  dataSource: any = [];
  displayedColumns: string[] = ['id', 'roomname', 'description','createBy', 'location','actions'];
  getListRoom(){
    this.roomService.getListRoomByUser(this.user.username).subscribe((data: any) =>{
      if(data){
        this.dataSource = data;
        console.log(this.dataSource);
      }
    })
  }
  deleteRoom(id){
    this.roomService.deleteRoom(id).subscribe((data: any) =>{
      if(data){
        this.dataSource = data;
        console.log("ok");
       this.getListRoom();
      }
    })  
  }   
   
}
