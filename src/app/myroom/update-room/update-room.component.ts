import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RoomService } from 'src/app/services/room.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-update-room',
  templateUrl: './update-room.component.html',
  styleUrls: ['./update-room.component.scss']
})
export class UpdateRoomComponent implements OnInit {

  constructor(
    public activatedRoute: ActivatedRoute,
    public roomService: RoomService,
    private location: Location,
    public router: Router 

  ) { }
  room_id;
  room: any;
  formUpdate: FormGroup
  ngOnInit(): void {
    this.formUpdate = new FormGroup({
      roomname: new FormControl("", [Validators.required]),
      description: new FormControl("", [Validators.required]),
      location: new FormControl("", [Validators.required]),
      createBy: new FormControl(),
    })
    this.getMyRoom();

  }
  getMyRoom() {
    this.room_id = +this.activatedRoute.snapshot.paramMap.get('room_id');
    console.log(this.room_id);
    this.roomService.getRoomById(this.room_id).subscribe((data: any) => {
      this.room = data
      console.log(this.room);
      this.formUpdate.setValue({
        roomname: this.room.roomname,
        description: this.room.description,
        location: this.room.location,
        createBy: this.room.createBy
      })
      console.log(this.formUpdate.value)
    });

  }
  goBack() {
    this.location.back();
  }
  updateRoom() {
    console.log(this.room_id);
    console.log(this.formUpdate.value);
    if (this.formUpdate.value) {
      this.roomService.updateRoom(this.room_id, this.formUpdate.value).subscribe((data: any) => {
        if (data) {
          this.router.navigate(["/your-room"]);
          console.log("ok");
        }
      });
    }
  }

}

