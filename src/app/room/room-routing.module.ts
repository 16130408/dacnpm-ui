import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoomComponent } from './room.component';
import { CreateComponent } from './create/create.component';
import { UpdateRoomComponent } from '../myroom/update-room/update-room.component';


const routes: Routes = [
  { path: '', component: RoomComponent },
  { path: 'create', component: CreateComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoomRoutingModule { }
