import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl , Validators} from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { RoomService } from 'src/app/services/room.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  constructor(
    public userService: UserService,
    private roomService: RoomService,
    private router: Router
  ) { }
  
  formCreateRoom: FormGroup
  ngOnInit(): void {
    this.formCreateRoom = new FormGroup({
      roomname: new FormControl("", [Validators.required]),
      description: new FormControl("", [Validators.required]),
      location: new FormControl("", [Validators.required]),
      createBy: new FormControl(),
    })
  }
  error: "";

  users: any = [
    { id: 1, username: "huy" },
    { id: 2, username: "Jack" },
  ]
  onSubmit() {
    // console.log("1231231")
    const user = this.userService.getUser();
    this.formCreateRoom.value['createBy'] = user.username;
    if (this.formCreateRoom.valid) {
      console.log(this.formCreateRoom.value)
      this.roomService.createRoom(this.formCreateRoom.value)
        .pipe(
          catchError((data) => {
            console.log(data)
            if (data.error) {
              this.error = data.error.message;
            } else {
              this.error = '';
            }
            return of();
          })
        )
        .subscribe((data: any) => {
          if (data) {
            this.router.navigate(["/room"])
            console.log(data);
          }
        })

    }
  }
}
