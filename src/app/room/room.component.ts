import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { RoomService } from '../services/room.service';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss']
})
export class RoomComponent implements OnInit {

  constructor(
    public userService: UserService,
    public roomService: RoomService
  ) { }

  ngOnInit(): void {
    this.getListRoom();
  }
  // dataSource:any = [
  //   {id: 1, roomname: 'Hydrogen', user_room: 1.0079},
  //   {id: 2, room_name: 'Helium', user_room: 4.0026},
  //   {id: 3, room_name: 'Lithium', user_room: 6.941},
  //   {id: 4, room_name: 'Beryllium', user_room: 9.0122},
  //   {id: 5, room_name: 'Boron', user_room: 10.811},
  //   {id: 6, room_name: 'Carbon', user_room: 12.0107},
  //   {id: 7, room_name: 'Nitrogen', user_room: 14.0067},
  //   {id: 8, room_name: 'Oxygen', user_room: 15.9994},
  //   {id: 9, room_name: 'Fluorine', user_room: 18.9984},
  // ];
  dataSource: any = [];
  displayedColumns: string[] = ['id', 'roomname', 'description','createBy', 'location','actions'];
  getListRoom(){
    this.roomService.getListRoom().subscribe((data: any) =>{
      if(data){
        this.dataSource = data;
        console.log(this.dataSource);
      }
    })
  }
  change(){}
}
