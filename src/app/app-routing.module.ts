import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogonComponent } from './auths/logon/logon.component';
import { RegisterComponent } from './auths/register/register.component';
import { MyNewsComponent } from './my-news/my-news.component';
import { HomeComponent } from './home/home.component';
import { MyroomComponent } from './myroom/myroom.component';
import { MyNewsModule } from './my-news/my-news.module';
import { NewsComponent } from './news/news.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'logon',
    pathMatch: 'full'
  },
  {
    path: 'logon',
    component: LogonComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  // {
  //   path: 'your-room',
  //   component: MyroomComponent
  // },
  { path: 'room', loadChildren: () => import('./room/room.module').then(m => m.RoomModule) },
  { path: 'your-room', loadChildren: () => import('./myroom/myroom.module').then(m => m.MyroomModule) },
  { path: 'news', loadChildren: () => import('./news/news.module').then(m => m.NewsModule) },

  { path: 'my-news', loadChildren: () => import('./my-news/my-news.module').then(m => m.MyNewsModule) },
  { path: 'your-room', loadChildren: () => import('./myroom/myroom.module').then(m => m.MyroomModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
