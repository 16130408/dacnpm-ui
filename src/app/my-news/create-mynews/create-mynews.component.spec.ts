import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMynewsComponent } from './create-mynews.component';

describe('CreateMynewsComponent', () => {
  let component: CreateMynewsComponent;
  let fixture: ComponentFixture<CreateMynewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateMynewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMynewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
