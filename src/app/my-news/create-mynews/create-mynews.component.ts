import { Component, OnInit } from '@angular/core';
import {  FormGroup, FormControl, Validators } from '@angular/forms';

import { UserService } from 'src/app/services/user.service';
import { MynewsService } from 'src/app/services/mynews.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-mynews',
  templateUrl: './create-mynews.component.html',
  styleUrls: ['./create-mynews.component.scss']
})
export class CreateMynewsComponent implements OnInit {


  error: " ";

  constructor(
    public userService: UserService,
    private myNewsService: MynewsService,
    private router: Router
  ) { }
  createForm: FormGroup
  image: File = null;
  formData: FormData;
  ngOnInit(): void {
    this.createForm = new FormGroup({
      title: new FormControl("", [Validators.required]),
      content: new FormControl("", [Validators.required]),
      image: new FormControl("")
    })

  }
  changeFile(files: File) {
    console.log("huy testing");
    // console.log(files[0])
    this.image = files[0];
  }

  onSubmit() {
    console.log("huy testing");
    this.formData = new FormData();
    this.formData.append("images",this.image);
    this.formData.append("content",this.createForm.controls['content'].value);
    this.formData.append("title",this.createForm.controls['title'].value);
    this.myNewsService.createMynews(this.formData).subscribe((data: any)=>{
      if(data.success == true){
        this.router.navigate(['/news'])
      }
    })
  }
  
}