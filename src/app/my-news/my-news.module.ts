import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import {MatButtonModule} from '@angular/material/button';
 import {MatTableModule} from '@angular/material/table';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';

import { MyNewsRoutingModule } from './my-news-routing.module';
import { MyNewsComponent } from './my-news.component';
import { CreateMynewsComponent } from './create-mynews/create-mynews.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
  declarations: [MyNewsComponent, CreateMynewsComponent],
  imports: [
    CommonModule,
    MyNewsRoutingModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatTableModule,MatIconModule
  ]
  ,
  // schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class MyNewsModule { }
