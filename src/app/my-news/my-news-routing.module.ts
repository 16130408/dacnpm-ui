import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyNewsComponent } from './my-news.component';
import { CreateMynewsComponent } from './create-mynews/create-mynews.component';


const routes: Routes = [
  {path: '', component: MyNewsComponent},
  // { path: 'create', component: CreateComponent },
  {path : 'create', component: CreateMynewsComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyNewsRoutingModule { }
