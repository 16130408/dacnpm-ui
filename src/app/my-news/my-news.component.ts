import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { MynewsService } from '../services/mynews.service';

@Component({
  selector: 'app-my-news',
  templateUrl: './my-news.component.html',
  styleUrls: ['./my-news.component.scss']
})
export class MyNewsComponent implements OnInit {
  title: String;
  date: Date;

  constructor(
    public userService: UserService,
    public mynewsService: MynewsService
  ) { }

  ngOnInit(): void {
    this.getListMynews();
  }
  dataSource: any = [];
  displayedColumns: String[] = ['id', 'title', 'content', 'image','actions'];

  getListMynews() {
    this.mynewsService.getListMynews().subscribe((data: any) => {
      if (data) {
        this.dataSource = data;
        console.log("data: ", this.dataSource);
      }
    })
  }
  deleteRoom(id) {
    console.log(id)
  }
}
